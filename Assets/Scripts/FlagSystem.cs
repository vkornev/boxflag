using UnityEngine;
using UnityEngine.AddressableAssets;
using Unity.Entities;
using Unity.Transforms;
using Unity.Rendering;
using Unity.Collections;
using Unity.Mathematics;
using System;
using System.Threading.Tasks;

public class FlagSystem : SystemBase
{
    private const string prefabLink = "Assets/Prefabs/Cube.prefab";
    private const int defaultHeight = 32;
    private const int defaultWidth = 32;

    private Action<int, float3> OnEntityRemoved;

    private EntityManager entityManager;
    private EntityArchetype cubeArchetype;

    private GameObject cube = null;

    private int Length;
    private int xDelta, yDelta;
    private Mesh mesh;
    private Material material;

    protected override void OnCreate()
    {
        base.OnCreate();

        InputManagerSystem.OnClick += Clicked;
        OnEntityRemoved += EntityRepop;

        Init();
    }

    protected override void OnUpdate()
    {
        Wave();
    }

    protected override void OnDestroy()
    {
        base.OnDestroy();

        InputManagerSystem.OnClick -= Clicked;
        OnEntityRemoved -= EntityRepop;
    }

    private void Init()
    {
        entityManager = World.EntityManager;
        cubeArchetype = entityManager.CreateArchetype(
            typeof(Translation),
            typeof(RenderMesh),
            typeof(LocalToWorld),
            typeof(RenderBounds),
            typeof(CubeColor),
            typeof(CubeData),
            typeof(RandomData));

        Addressables.LoadAssetAsync<GameObject>(prefabLink).Completed += (ao) =>
        {
            if (ao.Status != UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationStatus.Succeeded)
            {
                Debug.Log(ao.Status);
                return;
            }
            cube = ao.Result;
            InstantiateObjects();
        };
    }

    private void InstantiateObjects()
    {
        Length = defaultWidth * defaultHeight;
        xDelta = defaultWidth / 2;
        yDelta = defaultHeight / 2;
        Camera.main.orthographicSize = xDelta + 1;
        mesh = cube.GetComponent<MeshFilter>().sharedMesh;
        material = cube.GetComponent<Renderer>().sharedMaterial;

        NativeArray<Entity> entityArray = new NativeArray<Entity>(Length, Allocator.Temp);
        entityManager.CreateEntity(cubeArchetype, entityArray);
        for (int i = 0; i < Length; i++)
        {
            Entity entity = entityArray[i];
            int x = i / defaultHeight;
            int y = i % defaultHeight;
            float3 position = new float3(x - xDelta, y - yDelta, 0f);
            InitEntity(entity, i, position);
        }

        entityArray.Dispose();
    }

    private void Wave()
    {
        float time = UnityEngine.Time.time;
        Entities.ForEach((ref Translation translation, ref CubeData data, ref CubeColor cubeColor, ref RandomData rData) =>
        {
            int x = data.id / defaultHeight;
            int y = data.id % defaultHeight;
            float ponger = Mathf.PingPong(time + x * .2f, 1f);

            float3 tempPosition = data.defaultPosition;
            tempPosition.y += ponger;
            translation.Value = tempPosition;

            cubeColor.Value.x = rData.Value.NextFloat();
            cubeColor.Value.y = rData.Value.NextFloat();
            cubeColor.Value.z = rData.Value.NextFloat();
        }).ScheduleParallel();
    }

    private void Clicked(float3 position)
    {
        if (position.x < -xDelta - 1 || position.y < -yDelta || position.x > xDelta || position.y > yDelta)
            return;

        Entities.ForEach((Entity e, ref Translation translation, ref CubeData data) =>
        {
            float3 diff = translation.Value - position;
            float3 diffSqr = diff * diff;
            float distance = math.sqrt(diffSqr.x + diffSqr.y + diffSqr.z);
            if (distance < .5f)
            {
                int id = data.id;
                float3 defaultPosition = data.defaultPosition;
                entityManager.DestroyEntity(e);
                OnEntityRemoved?.Invoke(id, defaultPosition);
            }
        }).WithStructuralChanges().Run();
    }

    private async void EntityRepop(int id, float3 position)
    {
        await Task.Delay(5000);

        Entity entity = entityManager.CreateEntity(cubeArchetype);
        InitEntity(entity, id, position);
    }

    private void InitEntity(Entity entity, int id, float3 position)
    {
        entityManager.SetComponentData<CubeData>(entity, new CubeData { id = id, defaultPosition = position });
        entityManager.SetComponentData<RandomData>(entity, new RandomData { Value = Unity.Mathematics.Random.CreateFromIndex((uint)id) });
        entityManager.SetComponentData<Translation>(entity, new Translation { Value = position });
        entityManager.SetSharedComponentData<RenderMesh>(
            entity,
            new RenderMesh
            {
                mesh = mesh,
                material = material
            });
    }
}