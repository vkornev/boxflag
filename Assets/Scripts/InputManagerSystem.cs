﻿using System;
using UnityEngine;
using Unity.Entities;
using Unity.Mathematics;

public class InputManagerSystem : ComponentSystem
{
    public static event Action<float3> OnClick;
    public static event Action OnEscape;

    private Camera activeCamera = null;

    protected override void OnStartRunning()
    {
        base.OnStartRunning();

        activeCamera = Camera.main;
    }

    protected override void OnUpdate()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Vector3 worldPos = activeCamera.ScreenToWorldPoint(Input.mousePosition);
            float3 worldFloat3 = math.float3(worldPos.x, worldPos.y, 0f);
            OnClick?.Invoke(worldFloat3);
        }

        if (Input.GetKeyDown(KeyCode.Escape))
            OnEscape?.Invoke();
    }
}