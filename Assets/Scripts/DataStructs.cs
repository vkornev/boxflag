using System;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Rendering;

[Serializable]
public struct CubeData : IComponentData
{
    public int id;
    public float3 defaultPosition;
}

[Serializable]
[MaterialProperty("_BaseColor", MaterialPropertyFormat.Float4)]
public struct CubeColor : IComponentData
{
    public float4 Value;
}

[Serializable]
public struct RandomData : IComponentData
{
    public Unity.Mathematics.Random Value;
}
